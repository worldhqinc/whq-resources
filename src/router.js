import Vue from 'vue'
import Router from 'vue-router'
import LinkList from './components/LinkList'
import Categories from './components/Categories'
import Tags from './components/Tags'

Vue.use(Router)

export default new Router({
    mode: 'history',

    linkActiveClass: 'is-active',

    scrollBehavior () {
        return { x: 0, y: 0 }
    },

    routes: [
        {
            path: '/',
            redirect: '/all'
        },
        {
            path: '/all',
            name: 'all-links',
            component: LinkList
        },
        {
            path: '/category/:category',
            name: 'links-by-category',
            component: LinkList
        },
        {
            path: '/tag/:tag',
            name: 'links-by-tag',
            component: LinkList
        },
        {
            path: '/categories',
            name: 'categories',
            component: Categories
        },
        {
            path: '/tags',
            name: 'tags',
            component: Tags
        }
    ]
})
