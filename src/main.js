import Vue from 'vue'
import Raven from 'raven-js'
import RavenVue from 'raven-js/plugins/vue'
import Buefy from 'buefy'
import App from './App'
import router from './router'

Vue.config.productionTip = false

Vue.use(Buefy)

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: {App}
})

Raven
    .config('https://c84ed34ceb0f4c70876146b520afe323@sentry.io/171037', {
        ignoreUrls: ['http://localhost:8100']
    })
    .addPlugin(RavenVue, Vue)
    .install()
