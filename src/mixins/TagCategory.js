const TagCategory = {
    methods: {
        getCategoryName (key) {
            const matchingCategory = this.categories.find(category => category['.key'] === key)
            if (matchingCategory) {
                return matchingCategory.name
            }
        },

        getCategorySlug (key) {
            const matchingCategory = this.categories.find(category => category['.key'] === key)
            if (matchingCategory) {
                return matchingCategory.slug
            }
        },

        getTagName (key) {
            const matchingTag = this.tags.find(tag => tag['.key'] === key)
            if (matchingTag) {
                return matchingTag.name
            }
        },

        getTagSlug (key) {
            const matchingTag = this.tags.find(tag => tag['.key'] === key)
            if (matchingTag) {
                return matchingTag.slug
            }
        }
    }
}

export default TagCategory
