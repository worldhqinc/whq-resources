const StaggeredFade = {
    methods: {
        beforeEnter (el) {
            el.style.opacity = 0
        },

        enter (el) {
            const delay = el.dataset.index * 10
            setTimeout(() => {
                el.classList += ' animated fadeIn'
            }, delay)
        },

        leave (el) {
            const delay = el.dataset.index * 10
            setTimeout(() => {
                el.classList -= ' animated fadeIn'
                el.classList += ' animated fadeOut'
            }, delay)
        }
    }
}

export default StaggeredFade
