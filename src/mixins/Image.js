import {storage} from '../db'
import PlaceholderImage from './../assets/placeholder.svg'

const Image = {
    methods: {
        getImage (filename) {
            this.imageLoading = true
            if (filename) {
                const storageRef = storage.ref()
                storageRef.child(`images/${filename}`).getDownloadURL().then(url => {
                    this.image = url
                    this.imageLoading = false
                })
            } else {
                this.image = PlaceholderImage
                this.imageLoading = false
            }
        }
    }
}

export default Image
