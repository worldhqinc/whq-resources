import Firebase from 'firebase'

Firebase.database.enableLogging(false)

const config = {
    apiKey: 'AIzaSyA741DTZvu0QZeeBfdJoO0l7Vwo97-_iQA',
    authDomain: 'whq-resources.firebaseapp.com',
    databaseURL: 'https://whq-resources.firebaseio.com/',
    storageBucket: 'gs://whq-resources.appspot.com'
}

const app = Firebase.initializeApp(config)
const db = app.database()

export default db

export const refs = {
    categories: db.ref('categories'),
    tags: db.ref('tags'),
    links: db.ref('links')
}

export const storage = Firebase.storage()
