import Vue from 'vue'
import Vuex from 'vuex'
import {firebaseMutations, firebaseAction} from 'vuexfire'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        categories: [],
        tags: [],
        links: [],
        linksLoaded: false,
        user: null
    },

    mutations: {
        ...firebaseMutations,

        setUser (state, user) {
            state.user = user
        },

        setLinksLoaded (state, loaded) {
            state.linksLoaded = loaded
        }
    },

    getters: {
        categories: state => state.categories,
        tags: state => state.tags,
        links: state => state.links,

        getLinksByQuery: (state, getters) => query => {
            const searchTerms = query.split(' ')

            return state.links.filter(link => {
                for (let term of searchTerms) {
                    if (term.startsWith('#')) {
                        const tagQuery = term.split('#')[1]

                        if (link.tags) {
                            if (!link.tags.find(tagKey => {
                                const tag = state.tags.find(tag => tag['.key'] === tagKey)
                                if (tag) {
                                    return tag.name.toLowerCase().includes(tagQuery.toLowerCase())
                                } else {
                                    return false
                                }
                            })) {
                                return false
                            }
                        } else {
                            return false
                        }
                    } else {
                        const category = state.categories.find(category => category['.key'] === link.category)

                        if (!link.name.toLowerCase().includes(term.toLowerCase()) && !link.description.toLowerCase().includes(term.toLowerCase()) && !category.name.toLowerCase().includes(term.toLowerCase())) {
                            return false
                        }
                    }
                }

                return true
            })
        },

        getLinksByCategory: (state, getters) => categorySlug => {
            const category = state.categories.find(category => category.slug === categorySlug)
            if (category) {
                return state.links.filter(link => link.category === category['.key'])
            }
            return []
        },

        getLinksByTag: (state, getters) => tagSlug => {
            const tag = state.tags.find(tag => tag.slug === tagSlug)
            if (tag) {
                return state.links.filter(link => link.tags && link.tags.includes(tag['.key']))
            }
            return []
        }
    },

    actions: {
        setCategoriesRef: firebaseAction(({ bindFirebaseRef }, ref) => {
            bindFirebaseRef('categories', ref)
        }),

        setTagsRef: firebaseAction(({ bindFirebaseRef }, ref) => {
            bindFirebaseRef('tags', ref)
        }),

        setLinksRef: firebaseAction(({ bindFirebaseRef, commit }, ref) => {
            bindFirebaseRef('links', ref, {
                readyCallback: () => {
                    commit('setLinksLoaded', true)
                }
            })
        })
    }
})

export default store
