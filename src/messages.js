export const errorMessages = {
    'email-already-in-use': 'An account already exists with this email address.',
    'invalid-email': 'Email address is invalid.',
    'login-failed': 'Login failed.',
    'operation-not-allowed': 'This operation is not allowed',
    'popup-blocked': 'Looks like you have pop-ups blocked. You must enable them to sign in.',
    'too-many-requests': 'There have been too many requests to log in. Please wait and try again.',
    'user-disabled': 'This user has been disabled.',
    'user-not-found': 'User not found.',
    'weak-password': 'This password is not strong enough',
    'wrong-password': 'Incorrect password.'
}
